import React from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Home from '../containers/App';
import Nimbustech from '../containers/Nimbus'
import Telemetry from '../containers/Telemetry'
import Finance from '../containers/Finance'
import Logistic from '../containers/Logistic'
import Acerca from '../containers/Acerca'

import Layout from '../containers/Layout'

const App = () => (
    <BrowserRouter>
        <Layout>
            <Route exact path="/" component={Home}/>
            <Route exact path="/nimbustech" component={Nimbustech}/>
            <Route exact path="/telemetry" component={Telemetry}/>
            <Route exact path="/finance" component={Finance}/>
            <Route exact path="/logistic" component={Logistic}/>
            <Route exact path="/acercade" component={Acerca}/>
        </Layout>
    </BrowserRouter>
);

export default App;