import React from 'react';
import '../App.css';

import iconInstagram from '../static/iconos/i-instagram.svg'
import iconTwitter from '../static/iconos/i-twitter.svg'
import iconFacebook from '../static/iconos/i-facebook.svg'
import iconGeo from '../static/iconos/i-geo.svg'
import iconTelefono from '../static/iconos/i-telefono.svg'
import iconEmail from '../static/iconos/i-email.svg'

const Footer = () => {
    let today = new Date();
    var year = today.getFullYear();
    return(
        <div className="footer">
            <div className="row container-footer">
                <div className="w-33">
                    <div className="p-10">
                        <h4>Contacto</h4>
                        <div className="des-contacto">
                            <img className="icon-social-media" src={iconGeo}></img>
                            <p>Av. Felipe Pardo y Aliaga Nro. 640 Int. 1701 Urb. Santa Cruz Lima – San Isidro</p>
                        </div>
                        <div className="des-contacto">
                            <img className="icon-social-media" src={iconTelefono}></img>
                            <p>+51(01)628-5420</p>
                        </div>
                        <div className="des-contacto">
                            <img className="icon-social-media" src={iconEmail}></img>
                            <p>informes@nimbustechpe.com</p>
                        </div>
                    </div>
                </div>
                <div className="w-33">
                    <div className="p-10 border-r">
                        <h4>Menu</h4>
                        <div>
                            <p>Productos</p>
                            <ul>
                                <li>    
                                    <a href="#">Plataforma Nimbus</a>
                                </li>
                                <li>    
                                    <a href="#">TelemetryGo</a>
                                </li>
                                <li>    
                                    <a href="#">FinanceGO</a>
                                </li>
                                <li>    
                                    <a href="#">LogisticGO</a>
                                </li>
                            </ul>
                            <p><a href="#">Acerca de</a></p>
                            <p><a href="#">Contacto</a></p>
                        </div>
                    </div>
                </div>
                <div className="w-33">
                    <div className="p-10 border-r">
                        <h4>Redes</h4>
                        <a><img className="icon-social-media mr-40" src={iconInstagram}></img></a>
                        <a><img className="icon-social-media mr-40" src={iconTwitter}></img></a>
                        <a><img className="icon-social-media" src={iconFacebook}></img></a>
                    </div>
                </div>
                <div>
                    <div className="p-10">
                        <p>Nimbustech ® {year} - Todos los derechos reservados.</p>
                    </div>
                </div>
            </div>
        </div>
    )
};
export default Footer;