import React from 'react';
import logo from '../tel-log-n.svg';
import logoSolo from '../static/logos/nimbustech-color.svg'
import '../App.css';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';


import { AiOutlineMenu } from 'react-icons/ai';
import { FaUsers } from 'react-icons/fa';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
    list: {
      width: 250,
    },
    fullList: {
      width: 'auto',
    },
  });
const Header = () =>{
    const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <div className="list_options">
            <img src={logoSolo}/>
            <ul>
                <li>
                  <Link to="/" className="options_links_red"><b>Inicio</b></Link>
                </li>
                <li>
                    <b>Productos</b>
                    <ul>
                        <li><Link className="options_links" to="/nimbustech">Plataforma Nimbus</Link></li>
                        <li><Link className="options_links" to="/telemetry">TelemetryGO</Link></li>
                        <li><Link className="options_links" to="/finance">FinanceGO</Link></li>
                        <li><Link className="options_links" to="/logistic">LogisticGO</Link></li>
                    </ul>
                </li>
                <li>
                  <Link to="/" className="options_links_red"><b>Acerca de</b></Link>
                </li>
                <li>
                  <Link to="/" className="options_links_red"><b>Contacto</b></Link>
                </li>
            </ul>
        </div>
      </List>
    </div>
    );
    return(
        <div>
            <div className="header">
                <img src={logo}/>
                <span>
                <Button className="btn btn-primary mr-3 hidden-button-mobile">Ingreso de clientes</Button>
                <Button className="mr-3 show-button-mobile iconMenu"><FaUsers/></Button>
                {['right'].map((anchor) => (
                    <React.Fragment key={anchor}>
                    <Button className="iconMenu" onClick={toggleDrawer(anchor, true)}><AiOutlineMenu/></Button>
                    <Drawer anchor={anchor} open={state[anchor]} onClose={toggleDrawer(anchor, false)}>
                        {list(anchor)}
                    </Drawer>
                    </React.Fragment>
                ))}
                </span>
            </div>
            
        </div>
    )
};
export default Header;