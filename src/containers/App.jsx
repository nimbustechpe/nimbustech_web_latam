import React from 'react';
import FondoSecond from '../static/imagenes/productos-2@2x.png'
import FondoThird from '../static/imagenes/imagen-geo@2x.png'
import fiveGeo from '../static/imagenes/caracteristicas-geo2.svg'

import logoNimbustechBlanco from '../static/logos/nimbustech-blanco.svg'
import logoLogisticGoBlanco from '../static/logos/logisticgo-blanco.svg'
import logoFinanceGoBlanco from '../static/logos/financego-blanco.svg'
import logoTelemetryGoBlanco from '../static/logos/telemetrygo-blanco.svg'

import bloque1 from '../static/imagenes/bloque-n1@2x.png'
import bloque2 from '../static/imagenes/bloque-n2@2x.png'
import bloque3 from '../static/imagenes/bloque-n3@2x.png'
import bloque4 from '../static/imagenes/bloque-n4@2x.png'
import bloque5 from '../static/imagenes/bloque-n5@2x.png'

import bloque1Nimbus from '../static/imagenes/bloque1-nimbus.png'
import bloque3Logistic from '../static/imagenes/bloque3-logistic.png'
import bloque7Finance from '../static/imagenes/bloque7-finance.png'
import bloque6Telemetry from '../static/imagenes/bloque6-trackgo.png'

import logoGurtam from '../static/logos/gurtam.svg'
import logoErm from '../static/logos/erm@2x.png'
import logoDigevo from '../static/logos/digevo@2x.png'

import Slide from 'react-reveal/Slide'
import Zoom from 'react-reveal/Zoom'

import '../App.css';
const App = () => {
    return(
    <>
        <div className="bg-image">
            <div className="bg-text">
                <Slide left>
                    <h1>Líder en tecnologías GPS de localización en tiempo real</h1>
                    <p>Plataforma de localización y seguimiento personal o de flotas</p>
                </Slide>
            </div>
        </div>
        <div className="separacion">

        </div>
        <div className="bg-image-second">
            <div className="w-max">
                <Slide left>
                    <h1>Productos</h1>
                </Slide>
                <div className="info-productos">
                    <Zoom>
                        <img className="fondo-oculto" src={FondoSecond} alt="info"/>
                    </Zoom>
                </div>
                <div className="info-productos-mobile">
                    <div className="container-info row">
                        <div className="w-50">
                            <img src={logoNimbustechBlanco}/>
                            <p>Plataforma de geolocalizacion que gestiona y supervisa vehiculos o flotas, mejorando su productividad.</p>
                        </div>
                        <div className="w-50 info-productos-image">
                            <img src={bloque1Nimbus}/>
                        </div>
                    </div>
                    <div className="container-info row">
                        <div className="w-50">
                            <img src={logoLogisticGoBlanco}/>
                            <p>Sistema de seguimiento y control del proceso de despacho en la carga.</p>
                        </div>
                        <div className="w-50 info-productos-image">
                            <img src={bloque3Logistic}/>
                        </div>
                    </div>
                    <div className="container-info row">
                        <div className="w-50">
                            <img src={logoFinanceGoBlanco}/>
                            <p>Plataforma de geolocalizacion que gestiona y supervisa vehiculos o flotas, mejorando su productividad.</p>
                        </div>
                        <div className="w-50 info-productos-image">
                            <img src={bloque7Finance}/>
                        </div>
                    </div>
                    <div className="container-info row">
                        <div className="w-50">
                            <img src={logoTelemetryGoBlanco}/>
                            <p>Plataforma de geolocalizacion que gestiona y supervisa vehiculos o flotas, mejorando su productividad.</p>
                        </div>
                        <div className="w-50 info-productos-image">
                            <img src={bloque6Telemetry}/>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div className="separacion2">
            
        </div>
        <div className="separacion3">
            
        </div>
        <div className="p-0 row">
            <div className="w-50 bg-image-third">
                <img src={FondoThird}/>
            </div>
            <div className="w-50 bg-third-text">
                <div className="container-geo">
                    <Slide right>
                        <h1>Geolocalización</h1>
                        <h3>NimbusTech</h3>
                        <p>Plataforma de Gestión y control Vehicular que ofrece información completa para gestionar y supervisar los vehículos o flotas, y tomar decisiones que mejoren la productividad de la empresa, gracias a una completa Telemetría que permite evidenciar en tiempo real el estado mecánico y operativo.</p>
                    </Slide>
                    <Zoom>
                        <img src={fiveGeo}/>
                    </Zoom>
                </div>
            </div>
        </div>
        <div className="separacion2">
            
        </div>
        <div className="separacion3">
            
        </div>
        <div className="bg-image-fourth bg-image-telemetria">
            <Slide left>
                <h1>TELEMETRÍA</h1>
            </Slide>
            <div className="info-telemetria-bloque4">
                <Zoom>
                    <div className="descripcion-bloque4">
                        <h3>RPM</h3>
                        <p>Cuide el motor de sus vehículos. Obtenga completos reportes de eventos y frecuencias.</p>
                    </div>
                    <img className="fondo-oculto" src={bloque4} alt="info"/>
                </Zoom>
            </div>
            <div className="info-telemetria-bloque3">
                <Zoom>
                    <img className="fondo-oculto" src={bloque3} alt="info"/>
                    <div className="descripcion-bloque3">
                        <h3>ODÓMETRO</h3>
                        <p>Las mediciones de la distancia recorrida de su flota ya no es un estimado ni un aproximado, es un valor exacto.</p>
                    </div>
                </Zoom>
            </div>
            <div className="info-telemetria-bloque5">
                <Zoom>
                    <div className="descripcion-bloque5">
                        <h3>VELOCIDAD</h3>
                        <p>Evite multas para su flota, puede hacer seguimiento al comportamiento de sus conductores.</p>
                    </div>
                    <img className="fondo-oculto" src={bloque5} alt="info"/>
                </Zoom>
            </div>
            <div className="info-telemetria-bloque2">
                <Zoom>
                    <img className="fondo-oculto" src={bloque2} alt="info"/>
                    <div className="descripcion-bloque2">
                        <h3>TEMPERATURA</h3>
                        <p>Medición en la variación de distintos eventos significativos, ya sea en el Engine Coolant Temperature, Engine Fuel Temperature o Engine Oil Temperature.</p>
                    </div>
                </Zoom>
            </div>
            <div className="info-telemetria-bloque1">
                <Zoom>
                    <img className="fondo-oculto" src={bloque1} alt="info"/>
                    <div className="descripcion-bloque1">
                        <h3>USO DEL COMBUSTIBLE</h3>
                        <p>Completo control del gasto en cada uno de sus activos.</p>
                    </div>
                </Zoom>
            </div>
        </div>
        <div className="">
            <Slide left>
                <h2 className="p-10">Alianzas estratégicas</h2>
            </Slide>
            <div className="row">
                <div className="w-33 container-logo bg-gurtam">
                    <div className="p-10">
                        <Zoom>
                            <img src={logoGurtam} alt="Logo Gurtam" className="logo-gurtam"/>
                        </Zoom>
                    </div>
                </div>
                <div className="w-33 container-logo bg-erm">
                    <div className="p-10">
                        <Zoom>
                            <img src={logoErm} alt="Logo ERM"/>
                        </Zoom>
                    </div>
                </div>
                <div className="w-33 container-logo bg-digevo">
                    <div className="p-10">
                        <Zoom>
                            <img src={logoDigevo} alt="Logo Digevo"/>
                        </Zoom>
                    </div>
                </div>
            </div>
        </div>
    </>
)};
export default App;