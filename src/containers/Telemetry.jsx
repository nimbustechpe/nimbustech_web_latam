import React from 'react';

import LogoTelemetry from '../static/logos/telemetrygo-color.svg'

import imageMotor from '../static/telemetry/icon-motor.png'
import imageTemperatura from '../static/telemetry/icon-temperatura.png'
import imageDisel from '../static/telemetry/icon-disel.png'
import iconRpm from '../static/telemetry/icon-rpm.png'

import imageCompu from '../static/telemetry/compu@2x.png'

import imageDiagnostico from '../static/telemetry/circulo-cont.png'

import imageVelocidad from '../static/telemetry/cont-telemetry2@2x.png'
import imageCombustible from '../static/telemetry/cont-telemetry3@2x.png'
import imageRpm from '../static/telemetry/cont-telemetry4@2x.png'

import imageDashboard from '../static/telemetry/cont-telemetry5@2x.png'
import imageReportes from '../static/telemetry/cont-telemetry6@2x.png'
import imageAlertas from '../static/telemetry/cont-telemetry7@2x.png'

import '../styles/telemetry.css'
import '../App.css';

import Slide from 'react-reveal/Slide';
import Zoom from 'react-reveal/Zoom';

const Telemetry = () =>{
    return(
        <>
            <div className="bg-image-telemetry position-logo-text">
                <div className="row">
                    <div className="w-50 container-image-logo-tel">
                        <Slide left>
                            <img src={LogoTelemetry}/>
                        </Slide>
                    </div>
                    <div className="w-50 container-text-telemetry">
                        <Slide left>
                            <h1>Estado de tu flota en tiempo real</h1> 
                        </Slide>
                    </div>
                    <div className="container-icons-temeletry">
                        <div className="row">
                            <img src={imageMotor}/>
                            <img src={imageTemperatura}/>
                            <img src={imageDisel}/>
                            <img src={iconRpm}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="separacion-blanco">
            </div>
            <div className="separacion-blanco2">
            </div>
            <div className="separacion-blanco3">
            </div>
            <div className="bg-image-compu">
                <div className="row">
                    <div className="w-50 container-image-compu">
                        <Zoom >
                            <img src={imageCompu}/>
                        </Zoom>
                    </div>
                    <div className="w-50 container-text-exactitud">
                        <Slide right>
                            <h1>Exactitud de los datos siempre</h1> 
                            <p>Nuestra plataforma permite la medición, registro de procesos y eventos permitiendo monitorear en tiempo real las flotas o vehículos, lo que hace posible llevar un control más preciso de las métricas relacionadas con un activo.</p>
                        </Slide>
                    </div>
                </div>
            </div>
            <div className="bg-image-diagnostico">
                <div className="row">
                    <div className="container-text-diagnostico">
                        <Slide left>
                            <div className="text-diagnostico">
                                <h3>Diagnóstico Vehicular avanzada</h3>
                                <ul>
                                    <li>Lectura en tiempo real de protocolos OBD, FMS. </li>
                                    <li>Códigos y parámetros de  comunicación de la unidades de control. </li>
                                    <li>Scanner On Line, Alertas avanzadas por parámetros de funcionamiento o códigos de error DTC’s.</li>
                                </ul>
                            </div> 
                        </Slide>
                    </div>
                    <div className="image-diagnostico">
                        <Zoom>
                        <img src={imageDiagnostico}/>
                        </Zoom>
                    </div>
                </div>
            </div>
            <div className="bg-image-mediciones">
                <div className="row">
                    <div className="titulo-mediciones">
                        <h1>Mediciones importantes</h1>
                    </div>
                    <div className="container-text-mediciones">
                        <Slide left>
                            <div className="text-mediciones">
                                <div className="image-combustible">
                                    <img src={imageCombustible}/>
                                </div>
                                <div className="medicion-combustible">
                                    <h3>Combustible</h3>
                                    <p>El combustible es el material de principal gasto en empresas de transporte. Con esta tecnología, es posible medir la carga del motor, la aceleración, las frenadas muy bruscas y los niveles de combustible, y todo para hacer posible el ahorro de combustible en camiones.</p>
                           
                                </div>
                            </div> 
                        </Slide>
                        <Slide right>
                            <div className="text-mediciones">
                                <div className="image-combustible">
                                    <img src={imageRpm}/>
                                </div>
                                <div className="medicion-combustible">
                                    <h3>RPM</h3>
                                    <p>La reparación y mantenimiento del motor de los vehículos es uno de los grandes problemas en las flotas, pues su vida útil se ve mermada por las malas prácticas de conducción que desgastan el motor y otros componentes de los vehículos. Con las mediciones que hace en tiempo real TelemetryGo a los RPM del motor de sus unidades, reduce casi a 0 dicha problemática.</p>
                           
                                </div>
                            </div> 
                        </Slide>
                        <Slide left>
                            <div className="text-mediciones">
                                <div className="image-combustible">
                                    <img src={imageVelocidad}/>
                                </div>
                                <div className="medicion-combustible">
                                    <h3>Velocidad</h3>
                                    <p>La telemetría es utilizada en la prevención de accidentes, al monitorear en tiempo real la velocidad, frenado y arranque de una unidad. Al detectar que el límite de velocidad no ha sido respetado o que el conductor realiza frenados imprudentes, es posible alertarlo para corregir su desempeño.</p>
                           
                                </div>
                            </div> 
                        </Slide>
                    </div>
                </div>
                <div className="titulo-herramientas">
                        <h1>Herramientas útiles</h1>
                    </div>
                <div className="row container-herramientas">
                    <div className="w-50 image-dashboard">
                        <Zoom >
                            <img src={imageDashboard}/>
                        </Zoom>
                    </div>
                    <div className="w-50 text-herramientas">
                        <Slide right>
                            <h3>Dashboard</h3> 
                            <p> Completa visualización de su vehículo o flota, con mediciones evolutivas y ranking, que sumado a las comparativas con periódos anteriores se pueden apreciar las variaciones negativas o positivas en el comportamiento de sus activos en el tiempo.</p>
                        </Slide>
                    </div>
                    <div className="w-50 text-herramientas">
                        <Slide left>
                            <h3>Gráficos y Reportes</h3> 
                            <p>Graficación en tiempo real, detallando en frecuencias por horas y eventos de las mediciones de combustible, rpm, odómetro o temperatura. Además podrá extraer la información a través de una completa Reportería.</p>
                        </Slide>
                    </div>
                    <div className="w-50 image-dashboard">
                        <Zoom >
                            <img src={imageReportes}/>
                        </Zoom>
                    </div>
                    <div className="w-50 image-dashboard">
                        <Zoom >
                            <img src={imageAlertas}/>
                        </Zoom>
                    </div>
                    <div className="w-50 text-herramientas">
                        <Slide right>
                            <h3>Alertas</h3> 
                            <p>Módulo que permite la asignación de alertas, estableciendo parámetros que los activos no pueden infringir, en caso de que esto ocurra, se disparará un alerta ya sea al responsable de controlar la flota, y al conductor del vehículo para que este último corrija inmediatamente su actuar en la conducción.</p>
                        </Slide>
                    </div>
                </div>
            </div>
        </>
    )
};

export default Telemetry;