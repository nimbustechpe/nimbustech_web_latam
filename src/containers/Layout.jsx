import React from 'react';
import Header from '../components/Header'
import Footer from '../components/Footer'

const Layout = ({children}) => (
    <>
        <Header></Header>
        {children}
        <Footer></Footer>
    </>
)
export default Layout;