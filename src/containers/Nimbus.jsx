import React from 'react';

import LogoNimbus from '../static/logos/nimbustech-color.svg'

import imageMejorSolucion from '../static/nimbus/cont-nimbus@2x.png'

import iconTap from '../static/nimbus/tap.svg'
import iconGas from '../static/nimbus/gas-station.svg'
import iconRadar from '../static/nimbus/radar.svg'
import iconRoute from '../static/nimbus/route.svg'

import imageSeguridad from '../static/nimbus/seguridad@2x.png'
import imageRastreo from '../static/nimbus/rastreo@2x.png'
import imageControl from '../static/nimbus/control@2x.png'

import iconEcoDriving from '../static/nimbus/icon-ecodrive@2x.png'
import iconLogistic from '../static/nimbus/icon-logistic@2x.png'
import iconFleetrun from '../static/nimbus/icon-fleetrun@2x.png'
import iconNimbus from '../static/nimbus/icon-nimbus@2x.png'


import imageAccesible from '../static/nimbus/accessible.svg'

import '../styles/nimbus.css'
import '../App.css';

import Slide from 'react-reveal/Slide';
import Zoom from 'react-reveal/Zoom';

const Nimbus = () => {
    return(
    <>
        <div className="bg-image-nimbus position-logo-text">
            <div className="row">
                <div className="w-50 container-image-logo">
                    <Slide left>
                        <img src={LogoNimbus}/>
                    </Slide>
                </div>
                <div className="w-50 container-text-nimbus">
                    <Slide left>
                        <h1>Solución en la gestión y control Vehicular</h1> 
                    </Slide>
                </div>
                
            </div>
        </div>
        <div className="separacion-blanco">
        </div>
        <div className="separacion-blanco2">
        </div>
        <div className="separacion-blanco3">
        </div>
        <div className="bg-image-nimbus-second">
            <div className="row">
                <div className="container-imagen-mejorsolucion">
                    <Zoom>
                        <img src={imageMejorSolucion}/>
                    </Zoom>
                </div>
                <div className="container-text-mejorsolucion">
                    <Slide right>
                        <h1>La mejor solución</h1>
                        <div className="linea-mejorsolucion"></div>
                        <p>Ofrecemos información completa para gestionar y supervisar su vehículo o flotas, y tomar decisiones que mejoren la productividad de la empresa, gracias a una completa Telemetría que permite evidenciar en tiempo real el estado mecánico y operativo.</p>
                
                    </Slide>
                </div>
                <div className="w-50">
                    <Slide left>
                        <div className="icon-descripcion">
                            <img src={iconTap} />
                            <span><h3>Botón de Pánico y Corte de corriente</h3>Posiciona la ubicación del vehículo en ese momento, además de apagar el activo de forma remota.</span>               
                        </div>
                    </Slide>
                </div>
                <div className="w-50">
                    <Slide right>
                        <div className="icon-descripcion-2">
                            <img src={iconRadar} />
                            <span><h3>Control de geocercas</h3>Seguir vehículos y eventos dentro de un área especificada.</span>               
                        </div>
                    </Slide>
                </div>
                <div className="w-50">
                    <Slide left>
                        <div className="icon-descripcion-2">
                            <img src={iconGas} />
                            <span><h3>Combustible</h3>Medición del consumo de combustible con alta precisión.</span>               
                        </div>
                    </Slide>
                </div>
                <div className="w-50">
                    <Slide right>
                        <div className="icon-descripcion-2">
                            <img src={iconRoute} />
                            <span><h3>Planificar rutas</h3>Reducir el tiempo de viaje y el kilometraje.</span>               
                        </div>
                    </Slide>
                </div>
            </div>
        </div>
        <div className="separacion-blanco-f"></div>
        <div className="separacion-blanco-f2"></div>
        <div className="separacion-blanco-f3"></div>
        <div className="bg-image-nimbus-third">
            <Slide left>
                <h1>Beneficios</h1>
            </Slide>
            <div className="row">
                <div className="separacion-card">
                    <div className="container-card-info">
                        <div className="card-info">
                            <Zoom>
                            <img src={imageSeguridad}/>
                            <h3>Seguridad</h3>
                            <ul>
                                <li>
                                Control de uso no autorizado. 
                                </li>
                                <li>
                                Conocimiento del estado eléctrico del vehículo, rendimiento del combustible, batería y la detección de posibles eventos.
                                </li>
                            </ul>
                            </Zoom>
                        </div>
                    </div>
                </div>
                <div className="separacion-card">
                    <div className="container-card-info">
                        <div className="card-info">
                            <Zoom>
                            <img src={imageRastreo}/>
                            <h3>Rastreo vehicular</h3>
                            <ul>
                                <li>
                                Conocimiento del historial de viajes.
                                </li>
                                <li>
                                Navegación GPS.
                                </li>
                                <li>
                                Planificación de ruta óptima.
                                </li>
                                <li>
                                Visualización de Street View para ver posición exacta del vehículo.
                                </li>
                            </ul>
                            </Zoom>
                        </div>
                    </div>
                </div>
                <div className="separacion-card">
                    <div className="container-card-info">
                        <div className="card-info">
                            <Zoom>
                            <img src={imageControl}/>
                            <h3>Control</h3>
                            <ul>
                                <li>
                                Comportamiento del conductor.
                                </li>
                                <li>
                                Aviso de mantenimientos que requiere el vehículo.
                                </li>
                                <li>
                                En caso de robo corte segunda ignición.
                                </li>
                            </ul>
                            </Zoom>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="w-50">
                    <div className="container-image-accesible">
                        <Zoom>
                        <img src={imageAccesible} />
                        </Zoom>
                    </div>
                </div>
                <div className="w-50">
                    <div className="container-text-accesible">
                        <Slide right>
                        <h1>Aplicaciones</h1>
                        <p>La plataforma trae incorporadas un conjunto de aplicaciones adicionales que pueden solucionar varias necesidades para su flota, destacamos las siguientes.</p>
                    
                        </Slide>
                    </div>
                    <div className="mt-40">
                        <div className="icon-descripcion-2">
                            <Zoom>
                                <img src={iconEcoDriving} />
                            </Zoom>
                            <Slide right>
                            <span><h3>Eco Driving</h3>Análisis que permite controlar la calidad de conducción de su flota, volviéndola más eficiente.</span>               

                            </Slide>
                        </div>
                        <div className="icon-descripcion-2">
                            <Zoom>
                            <img src={iconLogistic} />
                            </Zoom>
                            <Slide right>
                                <span><h3>Logistics</h3>La solución permite realizar una amplia gama de tareas en el área de logística y entrega: planificación de rutas, optimización de procesos de transporte y ahorro de gastos.</span>               

                            </Slide>
                        </div>
                    </div>
                </div>
                <div className="w-50">
                    <div className="icon-descripcion-2 mt-20">
                        <Zoom>
                            <img src={iconFleetrun} />
                        </Zoom>
                        <Slide left>
                        <span><h3>Fleetrun</h3>Gestionar el proceso de mantenimiento técnico permitiendo evitar los fallos graves, minimizar los costos de mantenimiento y aumentar la eficacia de la flota.</span>               

                        </Slide>
                    </div>
                </div>
                <div className="w-50">
                    <div className="icon-descripcion-2 mt-20">
                        <Zoom>
                        <img src={iconNimbus} />
                        </Zoom>
                        <Slide right>
                        <span><h3>NimBus</h3>Solución para el transporte colectivo de pasajeros. crear paradas, optimizar rutas, distribuir vehículos entre rutinas, gestionar horarios y rastrear en tiempo real.</span>               

                        </Slide>
                    </div>
                </div>
            </div>
        </div>
    </>
)};

export default Nimbus;