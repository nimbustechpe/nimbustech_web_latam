import React from 'react';

import LogoLogisticCompleto from '../static/logos/logisticgo-color.svg'

import imageMachineLearning from '../static/logistic/machine-learning.svg'
import imagePath from '../static/logistic/path.svg'
import imageDash from '../static/logistic/dash.svg'
import imageCpu from '../static/logistic/cpu.svg'

import imageTrailer from '../static/logistic/trailer@2x.png'
import imageLaptopLog from '../static/logistic/laptop-log@2x.png'

import imageUbicacion from '../static/finance/cont-finance3.png'

import imageLaptop from '../static/finance/cont-finance7.png'



import '../styles/logistic.css'
import '../App.css';

import Slide from 'react-reveal/Slide';
import Zoom from 'react-reveal/Zoom';

const Logistic = () => {
    return(
        <>
            <div className="bg-image-logistic position-logo-text-log">
                <div className="row">
                    <div className="w-50 container-image-logo-log">
                        <Slide left>
                            <img src={LogoLogisticCompleto}/>
                        </Slide>
                    </div>
                    <div className="w-50 container-text-logistic">
                        <Slide left>
                            <h1>Control del proceso de despacho de carga</h1> 
                        </Slide>
                    </div>
                </div>
            </div>
            <div className="separacion-blanco">
            </div>
            <div className="separacion-blanco2">
            </div>
            <div className="separacion-blanco3">
            </div>
            <div className="container-carga">
                <div className="row">
                    <div className="container-carga-text">
                        <div className="carga-text">
                            <h1>Su carga a tiempo</h1>
                            <p>Permite gestionar el monitoreo de carga y tener los reportes necesarios para maximizar la eficiencia logística de las empresas generadoras de carga.</p>
                        </div>
                        <h3>Características destacadas</h3>
                        <div className="carga-text-caracteristicas">
                            <img src={imageMachineLearning}/>
                            <p>Creación de despachos manuales o automatizados.</p>
                        </div>
                        <div className="carga-text-caracteristicas">
                            <img src={imagePath}/>
                            <p>Multi-origen o multi-destino.</p>
                        </div>
                        <div className="carga-text-caracteristicas">
                            <img src={imageDash}/>
                            <p>Completa reportería</p>
                        </div>
                        <div className="carga-text-caracteristicas">
                            <img src={imageCpu}/>
                            <p>Múltiples dispositivos GPS.</p>
                        </div>
                    </div>
                    <div className="container-carga-image">
                        <div className="carga-image">
                            <div className="image-trailer">
                                <img src={imageTrailer}/>
                            </div>
                        </div>
                        <div className="carga-image2">
                            <div className="image-laptop-log">
                                <img src={imageLaptopLog}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="separacion-blanco-f"></div>
            <div className="separacion-blanco-f2"></div>
            <div className="separacion-blanco-f3"></div>
            <div className="container-destacado">
                <Slide left>
                    <h1>Beneficios</h1>
                </Slide>
                <div className="row">
                    <div className="container-destacado-logistic">
                        <div className="image-destacado-log">
                            <Zoom>
                                <img src={imageUbicacion}/>
                            </Zoom>
                        </div>
                        <div className="text-destacado">
                            <Zoom>
                                <h3>Optimización de la flota</h3>
                                <p>Al conocer bien las rutas, así como el ingreso a los lugares de destino y origen vía geocercas, se puede llevar un buen control que ayude a la distribución del tiempo y uso correcto de la flota.</p>
                            </Zoom>
                        </div>
                    </div>
                    <div className="container-destacado-logistic">
                        <div className="image-destacado-log">
                            <Zoom>
                                <img src={imageUbicacion}/>
                            </Zoom>
                        </div>
                        <div className="text-destacado">
                            <Zoom>
                                <h3>Alta disponibilidad en la nube</h3>
                                <p>La seguridad y respaldo de la información que se obtiene de los procesos de su flota son más rápidos y seguros funcionando en nuestra plataforma.</p>
                            </Zoom>
                        </div>
                    </div>
                    <div className="container-destacado-logistic">
                        <div className="image-destacado-log">
                            <Zoom>
                                <img src={imageUbicacion}/>
                            </Zoom>
                        </div>
                        <div className="text-destacado">
                            <Zoom>
                                <h3>Generación de KPI</h3>
                                <p>Ya sea para la distancia, tiempos de entrega, tiempo de un vehículo fuera de carretera, manera de conducción etc.</p>
                            </Zoom>
                        </div>
                    </div>
                </div>
            </div>
            <div className="bg-image-nimbus-second">
                <div className="row">
                    <div className="container-imagen-mejorsolucion-log">
                        <Zoom>
                            <img src={imageLaptop}/>
                        </Zoom>
                    </div>
                    <div className="container-text-mejorsolucion-log">
                        <Slide right>
                            <h1>Destacamos</h1>
                            <div className="linea-mejorsolucion"></div>
                            <p>LogisticGo cuenta con módulos que permiten la interacción con el usuario en el manejo de los viajes, su control en tiempo real, la información obtenida nutre la plataforma permitiendo la entrega de mediciones que servirán para tomar medidas que optimicen el comportamiento de la flota.</p>
                    
                        </Slide>
                    </div>
                    <div className="w-50">
                        <Slide left>
                            <div className="icon-descripcion-2">
                                <img src={imageDash} />
                                <span><h3>Dashboard</h3>Vistazo simple y comparativo de los viajes, destinos y clientes a través de mediciones gráficas.</span>               
                            </div>
                        </Slide>
                    </div>
                    <div className="w-50">
                        <Slide right>
                            <div className="icon-descripcion-2">
                                <img src={imageDash} />
                                <span><h3>Reportería</h3> Acceso rápido a toda la información de los viajes realizados, por realizar o que se están llevando a cabo.</span>               
                            </div>
                        </Slide>
                    </div>
                    <div className="w-50">
                        <Slide left>
                            <div className="icon-descripcion-2">
                                <img src={imageDash} />
                                <span><h3>Planificación</h3>Asignación de viajes, destinos, clientes, duración y seguimiento en tiempo real para el cumplimiento.</span>               
                            </div>
                        </Slide>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Logistic;