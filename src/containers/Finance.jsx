import React from 'react';

import LogoFinance from '../static/logos/financego-color.svg'

import LogoFinanceCompleto from '../static/finance/FINANCE_2_.svg'

import imageControl from '../static/finance/cont-finance2.png'

import imageUbicacion from '../static/finance/cont-finance3.png'
import imageDesconexion from '../static/finance/cont-finance4.png'
import imageRiesgo from '../static/finance/cont-finance5.png'
import imageDepreciacion from '../static/finance/cont-finance6.png'

import imageLaptop from '../static/finance/cont-finance7.png'

import imageDashboard from '../static/finance/dash.svg'
import imageCampana from '../static/finance/campania.svg'
import imageAlerta from '../static/finance/alertas.svg'
import imageDepre from '../static/finance/depreciacion.svg'

import '../styles/finance.css'
import '../App.css';

import Slide from 'react-reveal/Slide';
import Zoom from 'react-reveal/Zoom';

const Finance = () =>{
    return(
        <>
            <div className="bg-image-finance position-logo-text-f">
                <div className="row">
                    <div className="w-50 container-image-logo-f">
                        <Slide left>
                            <img src={LogoFinanceCompleto}/>
                        </Slide>
                    </div>
                    <div className="w-50 container-text-finance">
                        <Slide left>
                            <h1>Control de créditos para empresas Financiadoras</h1> 
                        </Slide>
                    </div>
                    
                </div>
            </div>
            <div className="separacion-blanco">
            </div>
            <div className="separacion-blanco2">
            </div>
            <div className="separacion-blanco3">
            </div>
            <div className="bg-image-finance-second">
                <div className="row">
                    <div className="container-imagen-control-f">
                        <Zoom>
                            <img src={imageControl}/>
                        </Zoom>
                    </div>
                    <div className="container-text-control-f">
                        <Slide right>
                            <h1>Control de créditos</h1>
                            <div className="linea-control-f"></div>
                            <div className="text-finance">
                            <p>FinanceGO, recoge las principales variables que minimizen el riesgo a la hora de manejar amplias carteras de clientes y estar al tanto de la utilización del activo, con un control de ubicaciones regulares, alertas de desconexión del equipo, alertas de vehículos con riesgo crediticio y la forma en que se deprecia. Brindando siempre una completa información para la administración y toma de decisiones críticas, como son los bajos costos operativos o control de cartera en mora.</p>
                            </div>
                    
                        </Slide>
                    </div>
                </div>
            </div>
            <div className="separacion-blanco-f"></div>
            <div className="separacion-blanco-f2"></div>
            <div className="separacion-blanco-f3"></div>
            <div className="container-destacado">
                <Slide left>
                    <h1>Destacado de FinanceGo</h1>
                </Slide>
                <div className="row">
                    <div className="container-destacado-finance">
                        <div className="image-destacado">
                            <Zoom>
                                <img src={imageUbicacion}/>
                            </Zoom>
                        </div>
                        <div className="text-destacado">
                            <Zoom>
                                <h3>Ubicación Regular</h3>
                                <p>Lectura constante de los lugares más comunes donde el cliente deja el vehículo. Facilitando encontrarlo en caso de morosidad.</p>
                            </Zoom>
                        </div>
                    </div>
                    <div className="container-destacado-finance">
                        <div className="image-destacado">
                            <Zoom>
                                <img src={imageDesconexion}/>
                            </Zoom>
                        </div>
                        <div className="text-destacado">
                            <Zoom>
                                <h3>Desconexión del activo</h3>
                                <p>Visualización de los vehículos. Desconectados o fuera de línea.</p>
                            </Zoom>
                        </div>
                    </div>
                    <div className="container-destacado-finance">
                        <div className="image-destacado">
                            <Zoom>
                                <img src={imageRiesgo}/>
                            </Zoom>
                        </div>
                        <div className="text-destacado">
                            <Zoom>
                                <h3>Riesgo crediticio</h3>
                                <p>La plataforma a través de la medición de variables entrega información de activos que representen un riesgo.</p>
                            </Zoom>
                        </div>
                    </div>
                    <div className="container-destacado-finance">
                        <div className="image-destacado">
                            <Zoom>
                                <img src={imageDepreciacion}/>
                            </Zoom>
                        </div>
                        <div className="text-destacado">
                            <Zoom>
                                <h3>Depreciación</h3>
                                <p>Cálculo que mide el deterioro del activo en el tiempo y alerta el momento en que el valor real del crédito fue inferior a la deuda</p>
                            </Zoom>
                        </div>
                    </div>
                </div>
            </div>
            <div className="separacion2-blanco-f3"></div>
            <div className="separacion2-blanco-f2"></div>
            <div className="separacion2-blanco-f"></div>
            <div className="container-modulos">
                <div className="row">
                    <div className="w-50">
                        <div className="container-image-accesible">
                            <Zoom>
                                <img src={imageLaptop} />
                            </Zoom>
                        </div>
                    </div>
                    <div className="w-50">
                        <div className="container-text-modulos-f">
                            <Slide right>
                                <h1>Módulos destacados</h1>
                            </Slide>
                        </div>
                        <div className="mt-40">
                            <div className="image-descripcion-2">
                                <Zoom>
                                    <img src={imageDashboard} />
                                </Zoom>
                                <Slide right>
                                    <span><h3>Tablero Principal</h3>Sirve para dar una mirada resumen de las mediciones más imporantantes como morosidad, perdida de la garantía o deuda vencida.</span>  
                                </Slide>             
                            </div>
                            <div className="image-descripcion-2">
                                <Zoom>
                                    <img src={imageCampana} />
                                </Zoom>
                                <Slide right>
                                    <span><h3>Campañas</h3>Sección en que se generan mensajes (sms, email ) que se puede hacer llegar a los clientes dueños de los créditos, mensajes que pueden ser informativos, ofertas, recordatorios etc.</span>  
                                </Slide>             
                            </div>
                        </div>
                    </div>
                    <div className="w-50">
                        <div className="image-descripcion-2 mt-20">
                            <Zoom>
                                <img src={imageAlerta} />
                            </Zoom>
                            <Slide left>
                                <span><h3>Alertas</h3>Se visualizan los créditos cuyos GPS dejaron de reportar ya sea que se encuentran En línea, Sin Conexión, con cobertura o Sin cobertura.</span> 
                            </Slide>              
                        </div>
                    </div>
                    <div className="w-50">
                        <div className="image-descripcion-2 mt-20">
                            <Zoom>
                                <img src={imageDepre} />
                            </Zoom>
                            <Slide right>
                                <span><h3>Depreciación</h3>Mirada detallada de cada crédito, a través de un gráfico evolutivo que permite ver el comportamiento que tiene el cliente con el vehículo y su respectiva desvalorización.</span>     
                            </Slide>          
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
};
export default Finance;